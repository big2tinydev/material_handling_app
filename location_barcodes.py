from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from pdf417gen import encode
from pdf417gen import render_image
from pdf417gen import render_svg
import os

root_path = "./media"


def create_dirs():
    location_options = [
        "G",
        "CPH",
        "BPH",
        "C",
        "CC",
        "B",
        "NWR",
        "F-SOUTH",
        "G-SOUTH",
        "G-NORTH",
        "D-FLOOR",
        "SAWS",
        "FORKTRUCKS",
        "CRANES",
        "HUBTEK",
        "profile"
    ]

    for loc in location_options:
        if not os.path.exists(f"{root_path}/{loc}"):
            os.makedirs(f"{root_path}/{loc}")


def create_g_location_barcode(location_type, isle, column, row):
    location_prefix = f"{location_type} "

    label = f"{location_type}{str(isle).zfill(2)}-{str(column).zfill(2)}-{str(row).zfill(2)}"
    text = f"1B{location_prefix}{str(isle).zfill(2)}-{str(column).zfill(2)}-{str(row).zfill(2)}"
    fontsize = 35

    def convert_code_to_words_and_save():
        # Convert to code words
        codes = encode(text, columns=3)
        image = render_image(codes, scale=2, ratio=10, padding=40, fg_color="black", bg_color="#ffffff00")

        image.save(f'{root_path}/{location_type}/{label}.png')
        return codes

    def generate_barcode_as_image():
        # Generate barcode as image
        myimage = Image.open(f'{root_path}/{location_type}/{label}.png')
        font_type = ImageFont.truetype('Arial Bold.ttf', fontsize)
        draw = ImageDraw.Draw(myimage)
        draw.text(xy=(80, 0), text=label, fill=(218, 165, 32), font=font_type)
        myimage.save(f'{root_path}/{location_type}/{label}.png')

    def create_svg():
        # Generate barcode as SVG
        svg = render_svg(convert_code_to_words_and_save().codes, scale=2, ratio=10)  # ElementTree object
        svg.write(f'{root_path}/{location_type}/{label}.svg')

    convert_code_to_words_and_save()
    generate_barcode_as_image()


def create_c_location_barcode(location_type, column, row):
    location_prefix = f"{location_type}"

    label = f"{location_type}{str(column).zfill(2)}-{str(row).zfill(2)}"
    text = f"1B{location_prefix}{str(column).zfill(2)}-{str(row).zfill(2)}"
    fontsize = 35

    def convert_code_to_words_and_save():
        # Convert to code words
        codes = encode(text, columns=3)
        image = render_image(codes, scale=2, ratio=10, padding=40, fg_color="black", bg_color="#ffffff00")

        image.save(f'{root_path}/{location_type}/{label}.png')
        return codes

    def generate_barcode_as_image():
        # Generate barcode as image
        myimage = Image.open(f'{root_path}/{location_type}/{label}.png')
        font_type = ImageFont.truetype('Arial Bold.ttf', fontsize)
        draw = ImageDraw.Draw(myimage)
        draw.text(xy=(80, 0), text=label, fill=(218, 165, 32), font=font_type)
        myimage.save(f'{root_path}/{location_type}/{label}.png')

    def create_svg():
        # Generate barcode as SVG
        svg = render_svg(convert_code_to_words_and_save().codes, scale=2, ratio=10)  # ElementTree object
        svg.write(f'{root_path}/{location_type}/{label}.svg')

    convert_code_to_words_and_save()
    generate_barcode_as_image()


def create_cph_location_barcode(location_type, column, row):
    global location_prefix

    if row < 100:
        location_prefix = f"C PH"
    elif row >= 100:
        location_prefix = f"CPH"

    label = f"{location_type}-{str(column).zfill(2)}-{str(row).zfill(2)}"
    text = f"1B{location_prefix}-{str(column).zfill(2)}-{str(row).zfill(2)}"
    fontsize = 35

    def convert_code_to_words_and_save():
        # Convert to code words
        codes = encode(text, columns=3)
        image = render_image(codes, scale=2, ratio=10, padding=40, fg_color="black", bg_color="#ffffff00")

        image.save(f'{root_path}/{location_type}/{label}.png')
        return codes

    def generate_barcode_as_image():
        # Generate barcode as image
        myimage = Image.open(f'{root_path}/{location_type}/{label}.png')
        font_type = ImageFont.truetype('Arial Bold.ttf', fontsize)
        draw = ImageDraw.Draw(myimage)
        draw.text(xy=(80, 0), text=label, fill=(218, 165, 32), font=font_type)
        myimage.save(f'{root_path}/{location_type}/{label}.png')

    def create_svg():
        # Generate barcode as SVG
        svg = render_svg(convert_code_to_words_and_save().codes, scale=2, ratio=10)  # ElementTree object
        svg.write(f'{root_path}/{location_type}/{label}.svg')

    convert_code_to_words_and_save()
    generate_barcode_as_image()


def create_bph_location_barcode(location_type, column, row):
    global location_prefix

    if row < 100:
        location_prefix = f"B PH"
    elif row >= 100:
        location_prefix = f"BPH"

    label = f"{location_type}-{str(column).zfill(2)}-{str(row).zfill(2)}"
    text = f"1B{location_prefix}-{str(column).zfill(2)}-{str(row).zfill(2)}"
    fontsize = 35

    def convert_code_to_words_and_save():
        # Convert to code words
        codes = encode(text, columns=3)
        image = render_image(codes, scale=2, ratio=10, padding=40, fg_color="black", bg_color="#ffffff00")

        image.save(f'{root_path}/{location_type}/{label}.png')
        return codes

    def generate_barcode_as_image():
        # Generate barcode as image
        myimage = Image.open(f'{root_path}/{location_type}/{label}.png')
        font_type = ImageFont.truetype('Arial Bold.ttf', fontsize)
        draw = ImageDraw.Draw(myimage)
        draw.text(xy=(80, 0), text=label, fill=(218, 165, 32), font=font_type)
        myimage.save(f'{root_path}/{location_type}/{label}.png')

    def create_svg():
        # Generate barcode as SVG
        svg = render_svg(convert_code_to_words_and_save().codes, scale=2, ratio=10)  # ElementTree object
        svg.write(f'{root_path}/{location_type}/{label}.svg')

    convert_code_to_words_and_save()
    generate_barcode_as_image()


def create_nwr_location_barcode(location_type, column, row):
    nwr_location_prefix = f"N WR"

    label = f"{location_type}-{str(column).zfill(2)}-{str(row).zfill(2)}"
    text = f"1B{nwr_location_prefix}-{str(column).zfill(2)}-{str(row).zfill(2)}"
    fontsize = 35

    def convert_code_to_words_and_save():
        # Convert to code words
        codes = encode(text, columns=3)
        image = render_image(codes, scale=2, ratio=10, padding=40, fg_color="black", bg_color="#ffffff00")

        image.save(f'{root_path}/{location_type}/{label}.png')
        return codes

    def generate_barcode_as_image():
        # Generate barcode as image
        myimage = Image.open(f'{root_path}/{location_type}/{label}.png')
        font_type = ImageFont.truetype('Arial Bold.ttf', fontsize)
        draw = ImageDraw.Draw(myimage)
        draw.text(xy=(80, 0), text=label, fill=(218, 165, 32), font=font_type)
        myimage.save(f'{root_path}/{location_type}/{label}.png')

    def create_svg():
        # Generate barcode as SVG
        svg = render_svg(convert_code_to_words_and_save().codes, scale=2, ratio=10)  # ElementTree object
        svg.write(f'{root_path}/{location_type}/{label}.svg')

    convert_code_to_words_and_save()
    generate_barcode_as_image()


def create_cc_location_barcode(location_type, row):
    location_prefix = f"{location_type}"

    label = f"{location_type}-{str(row).zfill(2)}"
    text = f"1B{location_prefix}-{str(row).zfill(2)}"
    fontsize = 35

    def convert_code_to_words_and_save():
        # Convert to code words
        codes = encode(text, columns=3)
        image = render_image(codes, scale=2, ratio=10, padding=40, fg_color="black", bg_color="#ffffff00")

        image.save(f'{root_path}/{location_type}/{label}.png')
        return codes

    def generate_barcode_as_image():
        # Generate barcode as image
        myimage = Image.open(f'{root_path}/{location_type}/{label}.png')
        font_type = ImageFont.truetype('Arial Bold.ttf', fontsize)
        draw = ImageDraw.Draw(myimage)
        draw.text(xy=(80, 0), text=label, fill=(218, 165, 32), font=font_type)
        myimage.save(f'{root_path}/{location_type}/{label}.png')

    def create_svg():
        # Generate barcode as SVG
        svg = render_svg(convert_code_to_words_and_save().codes, scale=2, ratio=10)  # ElementTree object
        svg.write(f'{root_path}/{location_type}/{label}.svg')

    convert_code_to_words_and_save()
    generate_barcode_as_image()


def create_all_c_barcodes(location_type, column, row):
    while row <= 11:
        create_c_location_barcode(location_type, column, row)
        row += 1


def create_all_b_barcodes(location_type, column, row):
    while row <= 11:
        create_c_location_barcode(location_type, column, row)
        row += 1


def create_all_cc_barcodes(location_type, row):
    while row <= 96:
        create_cc_location_barcode(location_type, row)
        row += 1


def create_all_cph_barcodes(location_type, column, row, max_row):
    while row <= max_row:
        create_cph_location_barcode(location_type, column, row)
        row += 1


def create_all_bph_barcodes(location_type, column, row, max_row):
    while row <= max_row:
        create_bph_location_barcode(location_type, column, row)
        row += 1


def all_cph_locations():
    create_all_cph_barcodes("CPH", 1, 1, 126)
    create_all_cph_barcodes("CPH", 2, 1, 102)
    create_all_cph_barcodes("CPH", 3, 1, 126)
    create_all_cph_barcodes("CPH", 4, 1, 126)
    create_all_cph_barcodes("CPH", 5, 1, 96)
    create_all_cph_barcodes("CPH", 6, 1, 126)
    create_all_cph_barcodes("CPH", 7, 1, 102)
    create_all_cph_barcodes("CPH", 8, 1, 96)
    create_all_cph_barcodes("CPH", 9, 1, 96)
    create_all_cph_barcodes("CPH", 10, 1, 96)
    create_all_cph_barcodes("CPH", 11, 1, 126)
    create_all_cph_barcodes("CPH", 12, 1, 126)
    create_all_cph_barcodes("CPH", 13, 1, 102)
    create_all_cph_barcodes("CPH", 14, 1, 126)
    create_all_cph_barcodes("CPH", 15, 1, 96)
    create_all_cph_barcodes("CPH", 16, 1, 126)
    create_all_cph_barcodes("CPH", 17, 1, 96)
    create_all_cph_barcodes("CPH", 18, 1, 126)
    create_all_cph_barcodes("CPH", 19, 1, 126)
    create_all_cph_barcodes("CPH", 20, 1, 98)
    create_all_cph_barcodes("CPH", 21, 1, 98)
    create_all_cph_barcodes("CPH", 22, 1, 126)
    create_all_cph_barcodes("CPH", 23, 1, 126)
    create_all_cph_barcodes("CPH", 24, 1, 98)

def all_bph_locations():
    create_all_bph_barcodes("BPH", 1, 1, 126)
    create_all_bph_barcodes("BPH", 2, 1, 126)
    create_all_bph_barcodes("BPH", 3, 1, 126)
    create_all_bph_barcodes("BPH", 4, 1, 40)
    create_all_bph_barcodes("BPH", 5, 1, 40)
    create_all_bph_barcodes("BPH", 6, 1, 40)
    create_all_bph_barcodes("BPH", 7, 1, 126)
    create_all_bph_barcodes("BPH", 8, 1, 126)
    create_all_bph_barcodes("BPH", 9, 1, 72)
    create_all_bph_barcodes("BPH", 10, 1, 90)
    create_all_bph_barcodes("BPH", 11, 1, 40)


def create_all_nwr_barcodes(location_type, column, row, max_row):
    while row <= max_row:
        create_nwr_location_barcode(location_type, column, row)
        row += 1


def all_nwr_locations():
    create_all_nwr_barcodes("NWR", 1, 1, 11)
    create_all_nwr_barcodes("NWR", 2, 1, 11)
    create_all_nwr_barcodes("NWR", 3, 1, 11)
    create_all_nwr_barcodes("NWR", 4, 1, 11)
    create_all_nwr_barcodes("NWR", 5, 1, 11)
    create_all_nwr_barcodes("NWR", 6, 1, 11)
    create_all_nwr_barcodes("NWR", 7, 1, 11)
    create_all_nwr_barcodes("NWR", 8, 1, 11)
    create_all_nwr_barcodes("NWR", 9, 1, 11)
    create_all_nwr_barcodes("NWR", 10, 1, 11)
    create_all_nwr_barcodes("NWR", 11, 1, 11)
    create_all_nwr_barcodes("NWR", 12, 1, 11)
    create_all_nwr_barcodes("NWR", 13, 1, 11)
    create_all_nwr_barcodes("NWR", 14, 1, 11)
    create_all_nwr_barcodes("NWR", 15, 1, 11)
    create_all_nwr_barcodes("NWR", 16, 1, 11)
    create_all_nwr_barcodes("NWR", 18, 1, 11)
    create_all_nwr_barcodes("NWR", 20, 1, 11)
    create_all_nwr_barcodes("NWR", 22, 1, 11)
    create_all_nwr_barcodes("NWR", 23, 1, 8)
    create_all_nwr_barcodes("NWR", 24, 1, 8)
    create_all_nwr_barcodes("NWR", 25, 1, 8)
    create_all_nwr_barcodes("NWR", 26, 1, 8)
    create_all_nwr_barcodes("NWR", 27, 1, 8)


def create_single_barcode_directory(directory, label):
    label = f"{label}"
    text = f"1B{label}"
    fontsize = 35

    def convert_code_to_words_and_save():
        # Convert to code words
        codes = encode(text, columns=3)
        image = render_image(codes, scale=2, ratio=10, padding=40, fg_color="black", bg_color="#ffffff00")

        image.save(f'{root_path}/{directory}/{label}.png')
        return codes

    def generate_barcode_as_image():
        # Generate barcode as image
        myimage = Image.open(f'{root_path}/{directory}/{label}.png')
        font_type = ImageFont.truetype('Arial Bold.ttf', fontsize)
        draw = ImageDraw.Draw(myimage)
        draw.text(xy=(80, 0), text=label, fill=(218, 165, 32), font=font_type)
        myimage.save(f'{root_path}/{directory}/{label}.png')

    def create_svg():
        # Generate barcode as SVG
        svg = render_svg(convert_code_to_words_and_save().codes, scale=2, ratio=10)  # ElementTree object
        svg.write(f'{root_path}/{directory}/{label}.svg')

    convert_code_to_words_and_save()
    generate_barcode_as_image()


def all_cc_locations(location_type):
    create_all_cc_barcodes(location_type, 1)


def all_b_locations(column):
    create_all_b_barcodes("B", column, 1)


def all_c_locations():
    create_all_c_barcodes("C", 12, 1)
    create_all_c_barcodes("C", 13, 1)
    create_all_c_barcodes("C", 14, 1)
    create_all_c_barcodes("C", 15, 1)
    create_all_c_barcodes("C", 19, 1)
    create_all_c_barcodes("C", 20, 1)


all_cph_locations()

