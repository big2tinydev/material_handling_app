# /locations/urls.py
from django.conf.urls.static import static
from django.urls import path

from base import settings
from . import views

app_name = 'locations'

urlpatterns = [
    path('', views.locations, name="home"),
    path('g/', views.gbay_locations, name="gbay_locations"),
    path('c/', views.c_locations, name="c_locations"),
    path('b/', views.b_locations, name="b_locations"),
    path('cc/', views.cc_locations, name="cc_locations"),
    path('bph/', views.bph_locations, name="bph_locations"),
    path('cph/', views.cph_locations, name="cph_locations"),
    path('nwr/', views.nwr_locations, name="nwr_locations"),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
