from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from .models import GBayLocation, CLocation, CCLocation, BLocation, BPHLocation, CPHLocation, NWRLocation


@admin.register(GBayLocation)
class GBayLocationAdmin(ImportExportModelAdmin):
    class Meta:
        model = GBayLocation


@admin.register(CLocation)
class CLocationAdmin(ImportExportModelAdmin):
    class Meta:
        model = CLocation


@admin.register(CCLocation)
class CCLocationAdmin(ImportExportModelAdmin):
    class Meta:
        model = CCLocation


@admin.register(BLocation)
class BLocationAdmin(ImportExportModelAdmin):
    class Meta:
        model = BLocation


@admin.register(CPHLocation)
class CPHLocationAdmin(ImportExportModelAdmin):
    class Meta:
        model = CPHLocation


@admin.register(BPHLocation)
class BPHLocationAdmin(ImportExportModelAdmin):
    class Meta:
        model = BPHLocation


@admin.register(NWRLocation)
class NWRLocationAdmin(ImportExportModelAdmin):
    class Meta:
        model = NWRLocation
