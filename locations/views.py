# /locations/views.py
from django.db.models import Q
from django.shortcuts import render

from .models import GBayLocation, CCLocation, CLocation, CPHLocation, BLocation, BPHLocation, NWRLocation


def locations(request):
    context = {}
    return render(request, 'locations/locations.html', context)


def gbay_locations(request):
    def is_valid_query_param(param):
        return param != "" and param is not None

    qs = GBayLocation.objects.all()
    contains_isle_query = request.GET.get('contains_isle')
    contains_col_query = request.GET.get('contains_col')
    contains_row_query = request.GET.get('contains_row')

    if is_valid_query_param(contains_isle_query):
        qs = qs.filter(Q(loc_isle__iexact=contains_isle_query)).distinct()
    if is_valid_query_param(contains_col_query):
        qs = qs.filter(Q(loc_column__iexact=contains_col_query)).distinct()
    if is_valid_query_param(contains_row_query):
        qs = qs.filter(Q(loc_row__iexact=contains_row_query)).distinct()

    context = {
        "qs": qs,
    }
    return render(request, 'locations/gbay_locations.html', context)


def c_locations(request):
    def is_valid_query_param(param):
        return param != "" and param is not None

    qs = CLocation.objects.all()
    contains_col_query = request.GET.get('contains_col')
    contains_row_query = request.GET.get('contains_row')

    if is_valid_query_param(contains_col_query):
        qs = qs.filter(Q(loc_column__iexact=contains_col_query)).distinct()
    if is_valid_query_param(contains_row_query):
        qs = qs.filter(Q(loc_row__iexact=contains_row_query)).distinct()

    context = {
        "qs": qs,
    }
    return render(request, 'locations/c_locations.html', context)


def b_locations(request):
    def is_valid_query_param(param):
        return param != "" and param is not None

    qs = BLocation.objects.all()
    contains_col_query = request.GET.get('contains_col')
    contains_row_query = request.GET.get('contains_row')

    if is_valid_query_param(contains_col_query):
        qs = qs.filter(Q(loc_column__iexact=contains_col_query)).distinct()
    if is_valid_query_param(contains_row_query):
        qs = qs.filter(Q(loc_row__iexact=contains_row_query)).distinct()

    context = {
        "qs": qs,
    }
    return render(request, 'locations/b_locations.html', context)


def cc_locations(request):
    def is_valid_query_param(param):
        return param != "" and param is not None

    qs = CCLocation.objects.all()
    contains_row_query = request.GET.get('contains_row')

    if is_valid_query_param(contains_row_query):
        qs = qs.filter(Q(loc_row__iexact=contains_row_query)).distinct()

    context = {
        "qs": qs,
    }
    return render(request, 'locations/cc_locations.html', context)


def bph_locations(request):
    def is_valid_query_param(param):
        return param != "" and param is not None

    qs = BPHLocation.objects.all()
    contains_col_query = request.GET.get('contains_col')
    contains_row_query = request.GET.get('contains_row')

    if is_valid_query_param(contains_col_query):
        qs = qs.filter(Q(loc_column__iexact=contains_col_query)).distinct()
    if is_valid_query_param(contains_row_query):
        qs = qs.filter(Q(loc_row__iexact=contains_row_query)).distinct()

    context = {
        "qs": qs,
    }
    return render(request, 'locations/bph_locations.html', context)


def cph_locations(request):
    def is_valid_query_param(param):
        return param != "" and param is not None

    qs = CPHLocation.objects.all()
    contains_col_query = request.GET.get('contains_col')
    contains_row_query = request.GET.get('contains_row')

    if is_valid_query_param(contains_col_query):
        qs = qs.filter(Q(loc_column__iexact=contains_col_query)).distinct()
    if is_valid_query_param(contains_row_query):
        qs = qs.filter(Q(loc_row__iexact=contains_row_query)).distinct()

    context = {
        "qs": qs,
    }
    return render(request, 'locations/bph_locations.html', context)


def nwr_locations(request):
    def is_valid_query_param(param):
        return param != "" and param is not None

    qs = NWRLocation.objects.all()
    contains_col_query = request.GET.get('contains_col')
    contains_row_query = request.GET.get('contains_row')

    if is_valid_query_param(contains_col_query):
        qs = qs.filter(Q(loc_column__iexact=contains_col_query)).distinct()
    if is_valid_query_param(contains_row_query):
        qs = qs.filter(Q(loc_row__iexact=contains_row_query)).distinct()

    context = {
        "qs": qs,
    }
    return render(request, 'locations/nwr_locations.html', context)
