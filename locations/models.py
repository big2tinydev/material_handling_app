from django.db import models
from django.template.defaultfilters import slugify


class Location(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    barcode = models.ImageField(blank=True, null=True)
    slug = models.SlugField(blank=True, null=True)


class GBayLocation(Location):
    loc_isle = models.IntegerField(null=True)
    loc_column = models.IntegerField(null=True)
    loc_row = models.IntegerField(null=True)

    class Meta:
        ordering = ['name']
        verbose_name_plural = "G Locations"
        verbose_name = "G Location"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):

        self.name = f"G {str(self.loc_isle).zfill(2)}-{str(self.loc_column).zfill(2)}-{str(self.loc_row).zfill(2)}"
        self.barcode = f"G/G{str(self.loc_isle).zfill(2)}-{str(self.loc_column).zfill(2)}-{str(self.loc_row).zfill(2)}.png"
        self.slug = slugify(self.name)
        super(GBayLocation, self).save(*args, **kwargs)


class CLocation(Location):
    loc_column = models.IntegerField(null=True)
    loc_row = models.IntegerField(null=True)

    class Meta:
        ordering = ['name']
        verbose_name_plural = "C Locations"
        verbose_name = "C Location"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):

        self.name = f"C{str(self.loc_column).zfill(2)}-{str(self.loc_row).zfill(2)}"
        self.barcode = f"C/C{str(self.loc_column).zfill(2)}-{str(self.loc_row).zfill(2)}.png"
        self.slug = slugify(self.name)
        super(CLocation, self).save(*args, **kwargs)


class BLocation(Location):
    loc_column = models.IntegerField(null=True)
    loc_row = models.IntegerField(null=True)

    class Meta:
        ordering = ['name']
        verbose_name_plural = "B Locations"
        verbose_name = "B Location"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):

        self.name = f"B{str(self.loc_column).zfill(2)}-{str(self.loc_row).zfill(2)}"
        self.barcode = f"B/B{str(self.loc_column).zfill(2)}-{str(self.loc_row).zfill(2)}.png"
        self.slug = slugify(self.name)
        super(BLocation, self).save(*args, **kwargs)


class CCLocation(Location):
    loc_row = models.IntegerField(null=True)

    class Meta:
        ordering = ['name']
        verbose_name_plural = "CC Locations"
        verbose_name = "CC Location"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):

        self.name = f"CC-{str(self.loc_row).zfill(2)}"
        self.barcode = f"CC/CC-{str(self.loc_row).zfill(2)}.png"
        self.slug = slugify(self.name)
        super(CCLocation, self).save(*args, **kwargs)


class BPHLocation(Location):
    loc_column = models.IntegerField(null=True)
    loc_row = models.IntegerField(null=True)

    class Meta:
        ordering = ['name']
        verbose_name_plural = "BPH Locations"
        verbose_name = "BPH Location"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):

        self.name = f"BPH-{str(self.loc_column).zfill(2)}-{str(self.loc_row).zfill(2)}"
        self.barcode = f"BPH/BPH-{str(self.loc_column).zfill(2)}-{str(self.loc_row).zfill(2)}.png"
        self.slug = slugify(self.name)
        super(BPHLocation, self).save(*args, **kwargs)


class CPHLocation(Location):
    loc_column = models.IntegerField(null=True)
    loc_row = models.IntegerField(null=True)

    class Meta:
        ordering = ['name']
        verbose_name_plural = "CPH Locations"
        verbose_name = "CPH Location"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):

        self.name = f"CPH-{str(self.loc_column).zfill(2)}-{str(self.loc_row).zfill(2)}"
        self.barcode = f"CPH/CPH-{str(self.loc_column).zfill(2)}-{str(self.loc_row).zfill(2)}.png"
        self.slug = slugify(self.name)
        super(CPHLocation, self).save(*args, **kwargs)


class NWRLocation(Location):
    loc_column = models.IntegerField(null=True)
    loc_row = models.IntegerField(null=True)

    class Meta:
        ordering = ['name']
        verbose_name_plural = "NWR Locations"
        verbose_name = "NWR Location"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):

        self.name = f"NWR-{str(self.loc_column).zfill(2)}-{str(self.loc_row).zfill(2)}"
        self.barcode = f"NWR/NWR-{str(self.loc_column).zfill(2)}-{str(self.loc_row).zfill(2)}.png"
        self.slug = slugify(self.name)
        super(NWRLocation, self).save(*args, **kwargs)
