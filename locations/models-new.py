from django.db import models
from django.template.defaultfilters import slugify
from location_barcodes import create_g_location_barcode
import os


class Location(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    barcode = models.ImageField(blank=True, null=True)
    slug = models.SlugField(blank=True, null=True)


class GBayLocation(Location):
    isle = models.IntegerField(null=True)
    column = models.IntegerField(null=True)
    row = models.IntegerField(null=True)

    class Meta:
        ordering = ['name']
        verbose_name_plural = "Locations"
        verbose_name = "location"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not os.path.exists('media'):
            create_g_location_barcode("G", self.isle, self.column, self.row)
        else:
            create_g_location_barcode("G", self.isle, self.column, self.row)

        self.name = f"G {str(self.isle).zfill(2)}-{str(self.column).zfill(2)}-{str(self.row).zfill(2)}"
        self.barcode = f"./media/G/G{str(self.isle).zfill(2)}-{str(self.column).zfill(2)}-{str(self.row).zfill(2)}.png"
        self.slug = slugify(self.name)
        super(GBayLocation, self).save(*args, **kwargs)


class CLocation(Location):
    column = models.IntegerField(null=True)
    row = models.IntegerField(null=True)

    class Meta:
        ordering = ['name']
        verbose_name_plural = "C Locations"
        verbose_name = "C Location"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name = f"C{str(self.column).zfill(2)}-{str(self.row).zfill(2)}"
        self.barcode = f"./media/C/C{str(self.column).zfill(2)}-{str(self.row).zfill(2)}.png"
        self.slug = slugify(self.name)
        super(CLocation, self).save(*args, **kwargs)


class CCLocation(Location):

    row = models.IntegerField(null=True)

    class Meta:
        ordering = ['name']
        verbose_name_plural = "CC Locations"
        verbose_name = "CC Location"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name = f"CC-{str(self.row).zfill(2)}"
        self.barcode = f"./media/CC/CC-{str(self.row).zfill(2)}.png"
        self.slug = slugify(self.name)
        super(CCLocation, self).save(*args, **kwargs)
