from django import forms
from . import models


class OrderForm(forms.ModelForm):
    class Meta:
        model = models.Order
        fields = [
            'wave',
            'ct',
            'rt',
            'due_date',
            'saw',
            'bin',
            'bin_isle',
            'bin_column',
            'bin_row',
            'material',
            'thickness',
            'width',
            'length',
            'pulled',
            'delivered',
            'stock_ran',
        ]
