from django.apps import AppConfig


class OrdersLiteConfig(AppConfig):
    name = 'orders_lite'
