from django import template

register = template.Library()


@register.inclusion_tag('orders_lite/components/nav_btn.html')
def orders_lite_links(icon, link, title):
    context = {
        'icon': icon,
        'link': link,
        'title': title
    }
    return context
