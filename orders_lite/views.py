# /orders_lite/views.py
from django.contrib.auth.decorators import login_required
from django.db.models import IntegerField
from django.db.models import Sum
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render

from orders_lite import forms
from orders_lite.models import Order


@login_required(login_url="accounts/login/")
def home(request):

    orders = Order.objects.all().order_by('wave__priority')
    picked_count = Order.objects.filter(picked=True).count()
    order_count = Order.objects.filter(picked=False, delivered=False, stock_ran=False).count()

    context = {
        "orders": orders,
        "picked_count": picked_count,
        "order_count": order_count,
    }
    return render(request, 'orders_lite/home.html', context)


@login_required(login_url="accounts/login/")
def order_create(request):
    if request.method == "POST":
        form = forms.OrderForm(request.POST or None, request.FILES)
        if form.is_valid():
            # Save to DB
            instance = form.save(commit=False)
            instance.employee = request.user
            instance.save()
            instance.save()
            return redirect('orders_lite:home')
    else:
        form = forms.OrderForm()

    context = {
        'form': form
    }
    return render(request, 'orders_lite/order_create.html', context)


def order_detail(request, slug):
    instance = Order.objects.get(slug=slug)
    context = {
        'instance': instance
    }
    return render(request, 'orders_lite/order_detail.html', context)


def order_delete(request, slug):
    instance = get_object_or_404(Order, slug=slug)
    if request.method == "POST":
        instance.delete()
        return redirect('orders_lite:home')

    context = {
        'instance': instance
    }
    return render(request, 'orders_lite/order_delete.html', context)


def home(request):
    orders = Order.objects.all().order_by('wave', 'due_date')
    context = {"orders": orders}
    return render(request, 'orders_lite/home.html', context)


@login_required(login_url="accounts/login/")
def order_edit(request, slug):
    instance = get_object_or_404(Order, slug=slug)
    form = forms.OrderForm(request.POST or None, instance=instance)
    if form.is_valid():
        # Save to DB
        instance = form.save(commit=False)
        instance.employee = request.user
        instance.save()
        return redirect('orders_lite:home')

    context = {
        'form': form,
        'instance': instance,
    }
    return render(request, 'orders_lite/order_edit.html', context)


@login_required(login_url="accounts/login/")
def order_duplicate(request, slug):
    instance = get_object_or_404(Order, slug=slug)
    form = forms.OrderForm(request.POST or None, request.FILES)
    if request.method == "POST":
        if form.is_valid():
            # Save to DB
            instance = form.save(commit=False)
            instance.employee = request.user
            instance.save()
            return redirect('orders_lite:home')
    else:
        form = forms.OrderForm(request.POST or None, request.FILES)

    context = {
        'form': form,
        'instance': instance,
    }
    return render(request, 'orders_lite/order_duplicate.html', context)
