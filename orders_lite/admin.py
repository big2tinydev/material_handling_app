from django.contrib import admin
from .models import Order, Wave, Material, Location, Saw
from import_export.admin import ImportExportModelAdmin


class OrderAdmin(admin.ModelAdmin):
    list_display = ['ct', 'rt', 'bin_name', 'wave', 'material']
    list_filter = ['ct', 'rt', 'bin_name', 'wave', 'material']
    exclude = ['slug', 'name']


admin.site.register(Order, OrderAdmin)


class SawAdmin(admin.ModelAdmin):
    list_display = ['name']
    list_filter = ['name']
    exclude = ['slug']


admin.site.register(Saw, SawAdmin)


class LocationAdmin(admin.ModelAdmin):
    list_display = ['bin_type']
    list_filter = ['bin_type']
    exclude = ['slug']


admin.site.register(Location, LocationAdmin)


class WaveAdmin(admin.ModelAdmin):
    list_display = ['name']
    list_filter = ['name']
    exclude = ['slug']


admin.site.register(Wave, WaveAdmin)


class MaterialAdmin(admin.ModelAdmin):
    list_display = ['material_name']
    list_filter = ['material_name']
    exclude = ['slug']


admin.site.register(Material, MaterialAdmin)
