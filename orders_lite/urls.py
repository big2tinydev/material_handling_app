# /orders_lite/urls.py
from django.conf.urls.static import static
from django.urls import path, include
from base import settings
from orders_lite.views import home, order_edit, order_duplicate, order_create, order_delete, order_detail

app_name = 'orders_lite'

urlpatterns = [
    path('', home, name="home"),
    path('create/', order_create, name='order_create'),
    path('<slug:slug>/', order_detail, name='order_detail'),
    path('<slug:slug>/edit/', order_edit, name='order_edit'),
    path('<slug:slug>/duplicate/', order_duplicate, name='order_duplicate'),
    path('<slug:slug>/delete/', order_delete, name='order_delete'),
]
