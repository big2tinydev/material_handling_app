from django.db import models
from django.template.defaultfilters import slugify
from django.utils.timezone import now as today
from orders_lite.create_ct_rt_barcodes import barcode, saw_barcode
from locations.models import BLocation, CLocation, CCLocation, GBayLocation, CPHLocation, BPHLocation, NWRLocation


class Material(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    material_name = models.CharField(max_length=200, blank=True, null=True)
    material_type = models.CharField(max_length=50, blank=True, null=True)
    density = models.FloatField(null=True)
    slug = models.SlugField(blank=True, null=True)

    class Meta:
        ordering = ['material_name']
        verbose_name_plural = "Materials"
        verbose_name = "Material"

    def __str__(self):
        return self.material_name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.material_name)
        super(Material, self).save(*args, **kwargs)


class Wave(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    color = models.CharField(max_length=10, blank=True, null=True)
    ship_time = models.TimeField(auto_now=False, auto_now_add=False)
    priority = models.IntegerField(null=True)
    slug = models.SlugField(blank=True, null=True)

    class Meta:
        ordering = ['priority']
        verbose_name_plural = "Waves"
        verbose_name = "Wave"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Wave, self).save(*args, **kwargs)


class Location(models.Model):
    BIN_TYPES = (
        ("G", "G Bay"),
        ("B", "B"),
        ("NWR", "NWR"),
        ("C", "C"),
        ("CC", "CC"),
        ("CPH", "CPH"),
        ("BPH", "BPH"),
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    bin_type = models.CharField(max_length=200, blank=True, null=True, choices=BIN_TYPES)
    if bin_type == "G":
        bin = models.ForeignKey(GBayLocation, on_delete=models.CASCADE)
    elif bin_type == "B":
        bin = models.ForeignKey(BLocation, on_delete=models.CASCADE)
    elif bin_type == "NWR":
        bin = models.ForeignKey(NWRLocation, on_delete=models.CASCADE)
    elif bin_type == "C":
        bin = models.ForeignKey(CLocation, on_delete=models.CASCADE)
    elif bin_type == "CC":
        bin = models.ForeignKey(CCLocation, on_delete=models.CASCADE)
    elif bin_type == "CPH":
        bin = models.ForeignKey(CPHLocation, on_delete=models.CASCADE)
    elif bin_type == "BPH":
        bin = models.ForeignKey(BPHLocation, on_delete=models.CASCADE)
    name = models.CharField(max_length=200, blank=True, null=True)
    slug = models.SlugField(blank=True, null=True)

    class Meta:
        ordering = ['id']
        verbose_name_plural = "Locations"
        verbose_name = "Location"

    def __str__(self):
        return self.bin_type

    def save(self, *args, **kwargs):
        self.name = f"{self.bin_type} {self.isle}-{self.column}-{self.row}"
        self.slug = slugify(self.bin_type)
        super(Location, self).save(*args, **kwargs)


class Saw(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        ordering = ['name']
        verbose_name_plural = "Saws"
        verbose_name = "Saw"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Saw, self).save(*args, **kwargs)


class Order(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    due_date = models.DateField(null=True)
    ct = models.IntegerField(null=True)
    ct_barcode = models.ImageField(blank=True, null=True)
    rt = models.IntegerField(null=True)
    rt_barcode = models.ImageField(blank=True, null=True)
    wave = models.ForeignKey(Wave, on_delete=models.CASCADE)
    bin_name = models.CharField(max_length=200, blank=True, null=True)
    bin = models.ForeignKey(Location, default=0, on_delete=models.CASCADE)
    bin_isle = models.IntegerField(null=True)
    bin_column = models.IntegerField(null=True)
    bin_row = models.IntegerField(null=True)
    bin_barcode = models.ImageField(blank=True, null=True)
    saw = models.ForeignKey(Saw, on_delete=models.CASCADE)
    saw_barcode = models.ImageField(blank=True, null=True)
    material = models.ForeignKey(Material, on_delete=models.CASCADE)
    thickness = models.FloatField(null=True, blank=True)
    width = models.FloatField(null=True, blank=True)
    length = models.FloatField(null=True, blank=True)
    pulled = models.BooleanField(default=False)
    delivered = models.BooleanField(default=False)
    stock_ran = models.BooleanField(default=False)
    slug = models.SlugField(blank=True, null=True)

    class Meta:
        ordering = ['due_date']
        verbose_name_plural = "Orders"
        verbose_name = "Order"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        barcode(self.ct, "CT")
        barcode(self.rt, "RT")
        bin_number = f"{str(self.bin_isle).zfill(2)}-{str(self.bin_column).zfill(2)}-{str(self.bin_row).zfill(2)}"
        barcode(bin_number, "G")
        saw_barcode(self.saw)

        self.thickness = 0
        self.width = 0
        self.length = 0

        self.ct_barcode = f"CT/CT{self.ct}.png"
        self.rt_barcode = f"RT/RT{self.rt}.png"
        self.saw_barcode = f"SAWS/{self.saw}.png"
        self.bin_barcode = f"G/{self.bin_name}.png"

        self.name = f"CT{self.ct} - RT{self.rt}"
        self.bin_name = f"{self.bin}{str(self.bin_isle).zfill(2)}-{str(self.bin_column).zfill(2)}-{str(self.bin_row).zfill(2)}"

        self.slug = slugify(self.name)
        super(Order, self).save(*args, **kwargs)
