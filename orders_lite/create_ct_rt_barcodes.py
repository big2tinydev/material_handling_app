import os

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from django.db.models.functions import datetime
from pdf417gen import encode, render_image
from base.settings import BASE_DIR

bc_text = ""
now = datetime.datetime.now()


class BarcodeVars:
    save_location = 'media'
    font_name = "Courier.ttf"
    red = 0
    green = 0
    blue = 0
    font_loc_x = 30
    font_loc_y = 0
    fontsize = 20
    fontsize2 = 12
    image_type = "png"
    num_of_cols = 3
    scale = 2
    ratio = 10
    padding = 30
    fg_color = "black"
    bg_color = "#000000"


def barcode(number, type_of):
    global bc_text
    directory = f"{BASE_DIR}/{BarcodeVars.save_location}/{type_of}/"
    label = f"{number}"
    filename = f"{type_of}{label}"

    if type_of == "CT":
        bc_text = f"{label}{type_of}"
    if type_of == "RT":
        bc_text = f"{type_of}{label}"
    if type_of == "G":
        bc_text = f"1B{type_of} {label}"

    codes = encode(bc_text, columns=BarcodeVars.num_of_cols)
    image = render_image(codes, scale=BarcodeVars.scale, ratio=BarcodeVars.ratio, padding=BarcodeVars.padding,
                         fg_color=BarcodeVars.fg_color)

    if not os.path.exists(f'{directory}/{filename}.{BarcodeVars.image_type}'):
        image.save(f'{directory}/{filename}.{BarcodeVars.image_type}')

        # myimage = Image.open(f'{directory}/{filename}.{BarcodeVars.image_type}')
        # font_type = ImageFont.truetype(BarcodeVars.font_name, BarcodeVars.fontsize)
        # draw = ImageDraw.Draw(myimage)
        # draw.text(xy=(BarcodeVars.font_loc_x, BarcodeVars.font_loc_y), text=filename,
        #           fill=(BarcodeVars.red, BarcodeVars.green, BarcodeVars.blue), font=font_type)
        # myimage.save(f'{directory}/{filename}.{BarcodeVars.image_type}')


def saw_barcode(saw_name):
    global bc_text
    directory = f"{BASE_DIR}/{BarcodeVars.save_location}/SAWS/"
    label = ""
    filename = f"{saw_name}"

    bc_text = f"1B{saw_name}"

    codes = encode(bc_text, columns=BarcodeVars.num_of_cols)
    image = render_image(codes, scale=BarcodeVars.scale, ratio=BarcodeVars.ratio, padding=BarcodeVars.padding,
                         fg_color=BarcodeVars.fg_color)

    if not os.path.exists(f'{directory}/{filename}.{BarcodeVars.image_type}'):
        image.save(f'{directory}/{filename}.{BarcodeVars.image_type}')

        # myimage = Image.open(f'{directory}/{filename}.{BarcodeVars.image_type}')
        # font_type = ImageFont.truetype(BarcodeVars.font_name, BarcodeVars.fontsize)
        # draw = ImageDraw.Draw(myimage)
        # draw.text(xy=(BarcodeVars.font_loc_x, BarcodeVars.font_loc_y), text=filename,
        #           fill=(BarcodeVars.red, BarcodeVars.green, BarcodeVars.blue), font=font_type)
        # myimage.save(f'{directory}/{filename}.{BarcodeVars.image_type}')
